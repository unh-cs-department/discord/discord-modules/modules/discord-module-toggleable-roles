interface ConfigRole {
    name: string,
    required: boolean
}

export default ConfigRole