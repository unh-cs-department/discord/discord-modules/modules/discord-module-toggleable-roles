interface ConfigToggleable {
    name: string;
    type: 0 | 1;
}

export default ConfigToggleable