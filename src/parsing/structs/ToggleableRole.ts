import Toggleable, {ToggleableTypes} from "./Toggleable"
import Role from "./Role"

interface ToggleableRole extends Toggleable {
    type: ToggleableTypes.ROLE;
    roles: {
        online: [
            Role
        ],
        offline: [
            Role
        ]
    }
}

export default ToggleableRole