interface Role {
    name: string;
    required: boolean;
}

export default Role