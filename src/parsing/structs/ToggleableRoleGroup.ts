import Toggleable, {ToggleableTypes} from "./Toggleable"

interface ToggleableRoleGroup extends Toggleable {
    type: ToggleableTypes.GROUP;
    choices: [
        string
    ]
}

export default ToggleableRoleGroup