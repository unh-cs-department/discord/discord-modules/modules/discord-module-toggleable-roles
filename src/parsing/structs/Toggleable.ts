enum ToggleableTypes {
    ROLE, GROUP
}

interface Toggleable {
    name: string;
    type: ToggleableTypes;
    parent: null | string;
}

export default Toggleable

export {
    ToggleableTypes
}