import IValidator from "../validation/IValidator"
import IParser from "./IParser"
import ToggleableConfigValidator from "../validation/ToggleableConfigValidator"
import Toggleable from "./structs/Toggleable"

import {ValidatorResult} from "jsonschema"
import ToggleableRoleTranslation from "./translations/ToggleableRoleTranslation"
import ConfigToggleableRole from "./structs/ConfigToggleableRole"
import ToggleableRoleGroupTranslation from "./translations/ToggleableRoleGroupTranslation"
import ConfigToggleableRoleGroup from "./structs/ConfigToggleableRoleGroup"
import ConfigToggleable from "./structs/ConfigToggleable"

class ToggleableConfigParser implements IParser<Toggleable> {
    private validator: IValidator
    public readonly choices: Toggleable[]

    constructor() {
        this.validator = new ToggleableConfigValidator()
        this.choices = []
    }

    public parse(data: object): Promise<Toggleable[]> {
        return new Promise<Toggleable[]>((resolve, reject) => {
            const validationResult: ValidatorResult = this.validator.validate(data)

            if (!validationResult.valid) return reject(new Error("Validation of roles.json failed."))

            this.flatten(data as ConfigToggleable[], null)

            return resolve(this.choices);
        })
    }

    private flatten(choices: ConfigToggleable[], parent: null | string): void {
        for (const c of choices) {
            let choice

            switch (c.type) {
                case 0:
                    const roleConfig = c as ConfigToggleableRole
                    choice = ToggleableRoleTranslation.translate(roleConfig)

                    break
                case 1:
                    const groupConfig = c as ConfigToggleableRoleGroup
                    choice = ToggleableRoleGroupTranslation.translate(groupConfig)

                    this.flatten(groupConfig.choices, groupConfig.name)

                    break
            }

            choice.parent = parent

            this.choices.push(choice)
        }
    }
}

export default ToggleableConfigParser