import {ToggleableTypes} from "../structs"
import ConfigToggleable from "../structs/ConfigToggleable"
import Toggleable from "../structs/Toggleable"

class ToggleableTranslation {
    public static translate(toggleable: ConfigToggleable): Toggleable {
        return {
            name: toggleable.name,
            type: Object.values(ToggleableTypes).at(toggleable.type),
            parent: null
        } as Toggleable
    }
}

export default ToggleableTranslation