import ConfigRole from "../structs/ConfigRole"
import Role from "../structs/Role"

class RoleTranslation {
    public static translate(toggleable: ConfigRole): Role {
        return {
            name: toggleable.name,
            required: toggleable.required
        } as Role
    }
}

export default RoleTranslation