import ConfigToggleableRoleGroup from "../structs/ConfigToggleableRoleGroup"
import ToggleableRoleGroup from "../structs/ToggleableRoleGroup"
import {ToggleableTypes} from "../structs"

class ToggleableRoleGroupTranslation {
    public static translate(group: ConfigToggleableRoleGroup) {
        return {
            name: group.name,
            type: ToggleableTypes.GROUP,
            choices: group.choices.map(c => c.name)
        } as ToggleableRoleGroup
    }
}

export default ToggleableRoleGroupTranslation