import {Client, Module, ModuleManifest} from "@unh-csonline/discord-modules"
import * as path from "path"
import ToggleablesRepository from "./repositories/ToggleablesRepository"
import OnlineCommand from "./commands/OnlineCommand"
import ToggleableConfigParser from "./parsing/ToggleableConfigParser"
import * as fs from "fs"
import OfflineCommand from "./commands/OfflineCommand"
import {I18n} from 'i18n';

export default class ToggleableRolesModule extends Module {
    public i18n: I18n
    public repository: ToggleablesRepository

    private readonly ABS_CONF_PATH: string

    constructor(client: Client, manifest: ModuleManifest) {
        super(client, manifest)

        this.repository = new ToggleablesRepository()
        this.i18n = new I18n()
        this.ABS_CONF_PATH = path.join(path.posix.resolve(), "config/discord-module-toggleable-roles")
    }

    public register(): Promise<void> {
        console.info("INFO: Registered discord-modules-toggleable-roles.")

        this.setupI18n()

        return Promise.all([
            this.setupRepository(),
            this.registerCommands()
        ])
            .catch(err => Promise.reject(err))
            .then(() => Promise.resolve())
    }

    private async setupRepository(): Promise<void> {
        const parser: ToggleableConfigParser = new ToggleableConfigParser()
        let data: Buffer

        try {
            data = fs.readFileSync(path.join(this.ABS_CONF_PATH, "roles.json"))
        } catch (err) {
            return Promise.reject(err)
        }

        const jsonData: object = await JSON.parse(data.toString())

        this.repository.set(await parser.parse(jsonData))
        return Promise.resolve()
    }

    private setupI18n() {
        this.i18n.configure({
            locales: ["en_US"],
            directory: path.join(this.ABS_CONF_PATH, "lang"),
            autoReload: true
        } as i18n.ConfigurationOptions)

        this.i18n.setLocale("en_US")
    }

    private async registerCommands(): Promise<void> {
        // online
        const onlineCommand = new OnlineCommand(this)
        await this.client.guildCommandManager.register(onlineCommand)

        const onlineCommandSelectMenuListener = new OnlineCommand.SelectMenuListener(onlineCommand, this)
        await this.client.listenerManager.register(onlineCommandSelectMenuListener)

        // offline
        const offlineCommand = new OfflineCommand(this)
        await this.client.guildCommandManager.register(offlineCommand)

        const offlineCommandSelectMenuListener = new OfflineCommand.SelectMenuListener(offlineCommand, this)
        await this.client.listenerManager.register(offlineCommandSelectMenuListener)
    }
}