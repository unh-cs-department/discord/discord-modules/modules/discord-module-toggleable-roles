const RoleSchema = {
    "id": "/Role",
    "type": "object",
    "properties": {
        "name": {
            "type": "string"
        }
    },
    "required": [
        "name"
    ]
}

const RolesOptionSchema = {
    "id": "/RolesOption",
    "type": "object",
    "properties": {
        "online": {
            "type": "array",
            "items": {
                "$ref": "/Role"
            }
        },
        "offline": {
            "type": "array",
            "items": {
                "$ref": "/Role"
            }
        }
    },
    "required": [
        "online",
        "offline"
    ]
}

const ToggleableOptionSchema = {
    "id": "/ToggleableOption",
    "type": "object",
    "properties": {
        "name": {
            "type": "string"
        },
        "type": {
            "enum": [
                0,
                1
            ]
        }
    },
    "allOf": [
        {
            "if": {
                "properties": {
                    "type": {
                        "const": 0
                    }
                }
            },
            "then": {
                "properties": {
                    "roles": {
                        "$ref": "/RolesOption"
                    }
                }
            }
        },
        {
            "if": {
                "properties": {
                    "type": {
                        "const": 1
                    }
                }
            },
            "then": {
                "properties": {
                    "choices": {
                        "type": "array",
                        "items": {
                            "$ref": "/ToggleableOption"
                        }
                    }
                },
                "required": [
                    "choices"
                ]
            }
        }
    ],
    "required": [
        "name",
        "type"
    ]
}

const ToggleableConfigSchema = {
    "type": "array",
    "items": {
        "$ref": "/ToggleableOption"
    }
}

export {
    RoleSchema,
    RolesOptionSchema,
    ToggleableOptionSchema,
    ToggleableConfigSchema
}