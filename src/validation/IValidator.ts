import {ValidatorResult} from "jsonschema"

interface IValidator {
    validate(json: object): ValidatorResult;

    isValid(json: object): boolean;
}

export default IValidator